<!--
  ~ Copyright (C) 2024 The Android Open Source Project
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~      http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->

<androidx.cardview.widget.CardView
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    app:cardBackgroundColor="@color/car_surface_container_high"
    app:cardCornerRadius="@dimen/media_card_card_radius"
    app:cardElevation="0dp">

    <androidx.constraintlayout.motion.widget.MotionLayout
        android:id="@+id/motion_layout"
        android:layout_height="match_parent"
        android:layout_width="match_parent"
        app:layoutDescription="@xml/panel_animation_motion_scene">

        <LinearLayout
            android:id="@+id/media_card_panel_content_container"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:layout_marginTop="@dimen/media_card_panel_content_margin_top"
            android:background="@color/car_surface_container_highest"
            android:orientation="vertical"
            app:layout_constraintBottom_toBottomOf="parent">
            <FrameLayout
                android:id="@+id/media_card_panel_handlebar"
                android:layout_width="match_parent"
                android:layout_height="@dimen/media_card_panel_handlebar_touch_target_height"
                android:paddingStart="@dimen/media_card_panel_handlebar_horizontal_padding"
                android:paddingEnd="@dimen/media_card_panel_handlebar_horizontal_padding"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintTop_toBottomOf="parent"
                app:layout_constraintBottom_toBottomOf="parent">
                <View
                    android:layout_width="match_parent"
                    android:layout_height="@dimen/media_card_panel_handlebar_height"
                    android:layout_gravity="center"
                    android:background="@drawable/media_card_panel_handlebar" />
            </FrameLayout>

            <androidx.viewpager2.widget.ViewPager2
                android:id="@+id/view_pager"
                android:background="@color/car_surface_container_highest"
                android:layout_width="match_parent"
                android:layout_height="match_parent" />
        </LinearLayout>

        <FrameLayout
            android:id="@+id/empty_panel"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:background="@color/car_surface_container_high"
            app:layout_constraintTop_toTopOf="parent"/>

        <ImageView
            android:id="@+id/album_art"
            android:layout_width="wrap_content"
            android:layout_height="@dimen/media_card_album_art_size"
            android:scaleType="fitCenter"
            android:adjustViewBounds="true"
            android:background="@android:color/transparent"
            android:layout_marginStart="@dimen/media_card_horizontal_margin"
            android:layout_marginEnd="@dimen/media_card_album_art_end_margin"
            android:layout_marginTop="@dimen/media_card_horizontal_margin"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintHorizontal_bias="0"
            app:layout_constrainedWidth="true"/>

        <ImageView
            android:id="@+id/media_widget_app_icon"
            android:layout_width="@dimen/media_card_app_icon_size"
            android:layout_height="@dimen/media_card_app_icon_size"
            android:clipToOutline="true"
            android:layout_marginEnd="@dimen/media_card_horizontal_margin"
            android:layout_marginTop="@dimen/media_card_horizontal_margin"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintEnd_toEndOf="parent" />

        <androidx.constraintlayout.widget.Guideline
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:id="@+id/guideline"
            app:layout_constraintGuide_begin="@dimen/media_card_text_view_guideline_start"
            android:orientation="horizontal"/>

        <TextView
            android:id="@+id/title"
            android:layout_height="wrap_content"
            android:layout_width="0dp"
            android:text="@string/metadata_default_title"
            android:textColor="@color/car_text_primary"
            android:gravity="center_vertical"
            android:maxLines="1"
            android:ellipsize="end"
            android:layout_marginTop="@dimen/media_card_view_separation_margin"
            android:layout_marginHorizontal="@dimen/media_card_horizontal_margin"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toBottomOf="@id/guideline"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintVertical_bias="0"/>

        <TextView
            android:id="@+id/subtitle"
            android:layout_height="wrap_content"
            android:layout_width="0dp"
            style="@style/TextAppearance.Car.Body.Small"
            android:textColor="@color/car_text_secondary"
            android:maxLines="1"
            android:ellipsize="end"
            android:layout_marginTop="@dimen/media_card_artist_top_margin"
            android:layout_marginHorizontal="@dimen/media_card_horizontal_margin"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toBottomOf="@id/title" />

        <SeekBar
            android:id="@+id/playback_seek_bar"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:paddingEnd="0dp"
            android:paddingStart="0dp"
            android:progressBackgroundTint="@color/car_surface_container_highest"
            android:progressDrawable="@drawable/media_card_seekbar_progress"
            android:progressTint="@color/car_primary"
            android:splitTrack="true"
            android:thumb="@drawable/media_card_seekbar_thumb"
            android:thumbTint="@color/car_on_surface"
            android:thumbOffset="0px"
            android:layout_marginTop="@dimen/media_card_view_separation_margin"
            android:layout_marginStart="@dimen/media_card_horizontal_margin"
            android:layout_marginEnd="@dimen/media_card_view_separation_margin"
            android:clickable="true"
            android:focusable="true"
            app:layout_goneMarginEnd="@dimen/media_card_horizontal_margin"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintEnd_toStartOf="@id/content_format"
            app:layout_constraintTop_toBottomOf="@id/subtitle" />

        <com.android.car.media.common.ContentFormatView
            android:id="@+id/content_format"
            android:layout_width="wrap_content"
            android:layout_height="@dimen/media_card_logo_size"
            android:layout_gravity="center_vertical"
            android:adjustViewBounds="true"
            android:scaleType="fitStart"
            app:logoTint="@color/car_on_surface_variant"
            app:logoSize="small"
            android:layout_marginEnd="@dimen/media_card_horizontal_margin"
            app:layout_constraintStart_toEndOf="@id/playback_seek_bar"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toTopOf="@id/playback_seek_bar"
            app:layout_constraintBottom_toBottomOf="@id/playback_seek_bar" />

        <ImageButton
            android:id="@+id/play_pause_button"
            android:layout_width="0dp"
            android:layout_height="@dimen/media_card_large_button_size"
            android:src="@drawable/ic_play_pause_selector"
            android:scaleType="center"
            android:tint="@color/car_surface_container_high"
            android:background="@drawable/pill_button_shape"
            android:backgroundTint="@color/car_primary"
            android:layout_marginBottom="@dimen/media_card_play_button_bottom_margin"
            app:layout_goneMarginEnd="@dimen/media_card_horizontal_margin"
            app:layout_goneMarginStart="@dimen/media_card_horizontal_margin"
            app:layout_constraintStart_toEndOf="@id/playback_action_id1"
            app:layout_constraintEnd_toStartOf="@id/playback_action_id2"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintVertical_bias="1" />

        <ImageButton
            android:id="@+id/playback_action_id1"
            android:layout_width="@dimen/media_card_large_button_size"
            android:layout_height="@dimen/media_card_large_button_size"
            android:scaleType="fitCenter"
            android:padding="@dimen/media_card_large_button_icon_padding"
            android:cropToPadding="true"
            android:tint="@color/car_on_surface_variant"
            android:background="@drawable/circle_button_background"
            android:layout_marginStart="@dimen/media_card_horizontal_margin"
            android:layout_marginEnd="@dimen/media_card_play_button_horizontal_margin"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintEnd_toStartOf="@id/play_pause_button"
            app:layout_constraintTop_toTopOf="@id/play_pause_button"
            app:layout_constraintBottom_toBottomOf="@id/play_pause_button"
            app:layout_constraintHorizontal_bias="0"/>

        <ImageButton
            android:id="@+id/playback_action_id2"
            android:layout_width="@dimen/media_card_large_button_size"
            android:layout_height="@dimen/media_card_large_button_size"
            android:scaleType="fitCenter"
            android:padding="@dimen/media_card_large_button_icon_padding"
            android:cropToPadding="true"
            android:tint="@color/car_on_surface_variant"
            android:background="@drawable/circle_button_background"
            android:layout_marginEnd="@dimen/media_card_horizontal_margin"
            android:layout_marginStart="@dimen/media_card_play_button_horizontal_margin"
            app:layout_constraintStart_toEndOf="@id/play_pause_button"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toTopOf="@id/play_pause_button"
            app:layout_constraintBottom_toBottomOf="@id/play_pause_button"
            app:layout_constraintHorizontal_bias="1" />

        <LinearLayout
            android:id="@+id/button_panel_background"
            android:layout_width="match_parent"
            android:layout_height="@dimen/media_card_bottom_panel_height"
            android:background="@drawable/media_card_button_panel_background"
            android:backgroundTint="@color/car_surface_container_highest"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintVertical_bias="1">
            <ImageButton
                android:id="@+id/overflow_button"
                android:layout_width="0dp"
                android:layout_height="@dimen/media_card_bottom_panel_button_size"
                android:layout_gravity="center"
                android:src="@drawable/ic_overflow_horizontal"
                android:layout_weight="1"
                style="@style/MediaCardPanelButtonStyle"/>
            <ImageButton
                android:id="@+id/queue_button"
                android:layout_width="0dp"
                android:layout_height="@dimen/media_card_bottom_panel_button_size"
                android:layout_gravity="center"
                android:src="@drawable/ic_queue"
                android:layout_weight="1"
                style="@style/MediaCardPanelButtonStyle"/>
            <ImageButton
                android:id="@+id/history_button"
                android:layout_width="0dp"
                android:layout_height="@dimen/media_card_bottom_panel_button_size"
                android:layout_gravity="center"
                android:src="@drawable/ic_history"
                android:layout_weight="1"
                style="@style/MediaCardPanelButtonStyle"/>
        </LinearLayout>
    </androidx.constraintlayout.motion.widget.MotionLayout>
</androidx.cardview.widget.CardView>
