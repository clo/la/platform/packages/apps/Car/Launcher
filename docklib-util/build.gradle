/*
 * Copyright (C) 2024 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

plugins {
    id 'com.android.library'
    id 'kotlin-android'
    id 'aconfig'
}

aconfig {
    aconfigDeclaration {
        packageName.set("com.android.car.dockutil")
        srcFile.setFrom(files("dock_flags.aconfig"))
    }
}

android {
    namespace 'com.android.car.dockutil'
    compileSdk gradle.ext.aaosTargetSDK

    defaultConfig {
        minSdk gradle.ext.aaosLatestSDK
        targetSdk gradle.ext.aaosLatestSDK
        versionCode gradle.ext.getVersionCode()
        versionName gradle.ext.getVersionName()

        testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
    }

    kotlinOptions {
        jvmTarget = '1.8'
    }

    sourceSets {
        main {
            manifest.srcFile 'AndroidManifest.xml'
            res.srcDirs = ['res']
            java.srcDirs = ['src']
        }

        androidTest {
            java.srcDirs += 'tests/src'
        }
    }

    testOptions {
        unitTests {
            includeAndroidResources = true
            returnDefaultValues = true
        }
    }

    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }

    lintOptions {
        disable 'PrivateResource'
    }
}

dependencies {
    implementation project(":libs:hidden-apis-compat:hidden-apis-disabled")
    implementation 'androidx.annotation:annotation:1.7.1'

    //Android Test dependencies
    androidTestImplementation project(":libs:hidden-apis-compat:hidden-apis-disabled")
    androidTestImplementation 'junit:junit:4.13.2'
    androidTestImplementation "org.mockito:mockito-android:5.10.0"
    androidTestImplementation "org.mockito:mockito-core:5.10.0"
    androidTestImplementation "org.mockito.kotlin:mockito-kotlin:3.2.0"
    androidTestImplementation "androidx.test:rules:1.5.0"
    androidTestImplementation 'androidx.test:runner:1.5.2'
    androidTestImplementation 'androidx.test.ext:junit:1.1.5'
    androidTestImplementation "com.google.truth:truth:1.3.0"
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}
